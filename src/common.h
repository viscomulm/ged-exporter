#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <string>
#include <cstdint>
#include <algorithm>

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float f32;

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::ivec4;
using glm::quat;

using glm::u8vec4;

inline std::string GetPath(const std::string& str)
{
    size_t found = str.find_last_of("/\\");
    return str.substr(0, found + 1);
}

inline std::string GetFile(const std::string& str)
{
    size_t found = str.find_last_of("/\\");
    return str.substr(found + 1);
}

inline std::string GetFileWithoutExtension(const std::string& str)
{
    size_t first = str.find_last_of("/\\");
    size_t last = str.find_last_of(".");
    return str.substr(first + 1, last - first - 1);
}

inline std::string GetFileExtension(const std::string& str)
{
    size_t found = str.find_last_of(".");
    return str.substr(found + 1);
}

inline std::string RemoveWhiteSpace(const std::string& str)
{
    std::string copy(str);
    copy.erase(std::remove_if(copy.begin(), copy.end(), isspace), copy.end());
    return std::move(copy);
}