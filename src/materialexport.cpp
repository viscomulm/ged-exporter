#include <cassert>
#include <fstream>
#include <assimp/scene.h>
#include "materialexport.h"
#include "common.h"

bool ExportGEDMaterial(const char* dst_file, const aiScene* scene)
{
    assert(dst_file);
    assert(scene);

    std::ofstream file(dst_file, std::fstream::out);
    if (!file.is_open()) {
        printf("Could not open or create material file! \n");
        exit(-1);
    }

    printf("Exporting material %s \n", dst_file);

    file << "# GED Material file, generated through exporter" << std::endl;
    file << "# To be used with " << GetFileWithoutExtension(dst_file) + ".gmsh & gmdl" << std::endl << std::endl;

    for (u32 i = 0; i < scene->mNumMaterials; i++)
    {
        aiString ai_diff, ai_norm, ai_height, ai_spec, ai_shin, ai_emis, ai_opac, ai_name;
        const aiMaterial* aiMat = scene->mMaterials[i];
        aiMat->Get(AI_MATKEY_NAME, ai_name);

        aiMat->GetTexture(aiTextureType_DIFFUSE, 0, &ai_diff);
        aiMat->GetTexture(aiTextureType_NORMALS, 0, &ai_norm);
        aiMat->GetTexture(aiTextureType_HEIGHT, 0, &ai_height);
        aiMat->GetTexture(aiTextureType_SHININESS, 0, &ai_shin);
        aiMat->GetTexture(aiTextureType_EMISSIVE, 0, &ai_emis);
        aiMat->GetTexture(aiTextureType_OPACITY, 0, &ai_opac);

        std::string name(ai_name.C_Str());
        if (name == "")
            name = "material" + std::to_string(i);
        name = RemoveWhiteSpace(name);
        file << "MATERIAL " << name << std::endl;

        if (ai_diff.length > 0)
            file << "BASE_COLOR " << ai_diff.C_Str() << " 1.0 1.0 1.0 1.0" << std::endl;

        if (ai_norm.length > 0)
            file << "NORMALS " << ai_norm.C_Str() << " 1.0 1.0 1.0 1.0" << std::endl;
        
        if (ai_shin.length > 0)
            file << "SMOOTHNESS " << ai_shin.C_Str() << " 1.0 1.0 1.0 1.0" << std::endl;

        if (ai_height.length > 0)
            file << "HEIGHT " << ai_height.C_Str() << " 1.0 1.0 1.0 1.0" << std::endl;

        if (ai_opac.length > 0)
            file << "ALPHA " << ai_opac.C_Str() << " 1.0 1.0 1.0 1.0" << std::endl;

        file << std::endl;
    }

    return false;
}