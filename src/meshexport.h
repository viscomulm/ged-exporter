#pragma once

#include <vector>
#include <string>

struct aiScene;
bool ExportGEDMesh(const char* dst_file, const aiScene* scene, const std::vector<std::string>& mesh_names);