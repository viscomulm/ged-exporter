#include <cstdio>
#include "export.h"

int main(const int argc, const char* argv[])
{
    const char* file;
    if (argc < 2) {
        file = "sponza3.obj";
        //printf("No file was given, terminating. \n");
        //return 0;
    }
    else
        file = argv[1];
        
    ExportGED(file);

    return 0;
}