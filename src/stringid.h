#pragma once

#include <string>
#include "common.h"

namespace ged
{
    class StringID
    {
    public:
        StringID(const char* string);
        StringID(const std::string& string);
        StringID(const StringID& other);
        StringID(const StringID&& other);

        const char* c_str() const { return m_string.c_str(); }
        const std::string& string() const { return m_string; }
        u32 id() const { return m_id; }

        inline bool operator < (const StringID& other) const
        {
            return m_id < other.m_id;
        }

        inline bool operator > (const StringID& other) const
        {
            return m_id > other.m_id;
        }

        inline bool operator == (const StringID& other) const
        {
            return m_id == other.m_id;
        }

    private:
        std::string m_string;
        u32 m_id;
    };
}