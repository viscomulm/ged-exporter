#include <vector>
#include <unordered_map>
#include <string>
#include <fstream>
#include <algorithm>
#include <assimp/scene.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/packing.hpp>
#include "common.h"
#include "meshexport.h"
#include "meshformat.h"


inline void CopyAiMatrixToGLM(const aiMatrix4x4 *from, glm::mat4 &to)
{
    // TODO Possibly just read and transpose

    to[0][0] = from->a1; to[1][0] = from->a2;
    to[2][0] = from->a3; to[3][0] = from->a4;
    to[0][1] = from->b1; to[1][1] = from->b2;
    to[2][1] = from->b3; to[3][1] = from->b4;
    to[0][2] = from->c1; to[1][2] = from->c2;
    to[2][2] = from->c3; to[3][2] = from->c4;
    to[0][3] = from->d1; to[1][3] = from->d2;
    to[2][3] = from->d3; to[3][3] = from->d4;
}

void InsertWeight(SkinnedVertex& v, i32 index, float weight)
{
    assert(-1 < index && index < 256);
    for (int i = 0; i < 4; i++)
    {
        if (v.indices[i] == 0 && v.weights[i] == 0) {
            v.indices[i] = index;
            v.weights[i] = glm::packUnorm1x8(weight);
            return;
        }
    }
}

i32 GetNodeIndex(const std::unordered_map<std::string, i32>& id_map, const std::string& id)
{
    auto it = id_map.find(id);
    if (it != id_map.end())
        return it->second;

    return -1;
}

void ReadBoneHierarchy(std::vector<Bone>& bone_data,
                       const aiNode* node,
                       i32 parent,
                       glm::mat4 matrix)
{
    if (node == nullptr)
        return;

    std::string name(node->mName.C_Str());

    if (name != "<MD5_Root>" &&
        name != "<MD5_Mesh>" &&
        name != "<MD5_Hierarchy>")
    {
        glm::mat4 M;
        CopyAiMatrixToGLM(&node->mTransformation, M);

        matrix = matrix * M;
        glm::mat4 inv_mat = glm::inverse(matrix);

        Bone bone;
        u32 len = std::min(static_cast<u32>(name.length()), 32U);
        strncpy(bone.name, name.data(), len);
        bone.name[len] = '\0';
        bone.q = glm::quat_cast(inv_mat);
        bone.t = glm::vec3(inv_mat[3]);
        bone.s = glm::vec3(glm::length(inv_mat[0]), glm::length(inv_mat[1]), glm::length(inv_mat[2]));
        bone.parent = parent;

        printf("Bone[%u]: '%s' %i\n", (u32)bone_data.size(), bone.name, bone.parent);
        //std::cout << glm::to_string(inv_mat) << "\n\n";

        parent = bone_data.size();
        bone_data.push_back(bone);

        //std::cout << name << "\n";
        //std::cout << glm::to_string(M) << "\n";
    }

    for (u32 i = 0; i < node->mNumChildren; i++)
        ReadBoneHierarchy(bone_data, node->mChildren[i], parent, matrix);
}

void PrintBoneHierarchy(const aiNode* node)
{
    if (node == nullptr)
        return;

    printf("%s \n", node->mName.C_Str());

    for (u32 i = 0; i < node->mNumChildren; i++)
        PrintBoneHierarchy(node->mChildren[i]);
}

bool ExportGEDMesh(const char* dst_file, const aiScene* scene, const std::vector<std::string>& mesh_names)
{
    assert(dst_file);
    assert(scene);

    std::ofstream file(dst_file, std::ofstream::binary);
    if (!file.is_open()) {
        printf("Could not open or create model file! \n");
        exit(-1);
    }

    printf("Exporting mesh %s \n", dst_file);

    // printf("num_meshes %i \n", scene->mNumMeshes);

    /* Calculate data required */
    u32 num_groups = 0;
    u32 num_vertices = 0;
    u32 num_indices = 0;
    u32 num_bones = 0;

    for (unsigned int i = 0; i < scene->mNumMeshes; i++)
    {
        const aiMesh* mesh = scene->mMeshes[i];
        num_groups++;
        num_vertices += mesh->mNumVertices;
        num_indices += mesh->mNumFaces * 3;
        num_bones += mesh->mNumBones;
    }

    std::vector<Group> group_data;
    std::vector<Vertex> vertex_data;
    std::vector<SkinnedVertex> skinned_vertex_data;
    std::vector<u32> index_data;
    std::vector<Bone> bone_data;

    std::unordered_map<std::string, i32> id_map;

    group_data.reserve(num_groups);
    vertex_data.reserve(num_vertices);
    skinned_vertex_data.reserve(num_vertices);
    index_data.reserve(num_indices);
    bone_data.reserve(num_bones);

    u32 vertex_offset = 0;
    for (u32 m_idx = 0; m_idx < scene->mNumMeshes; m_idx++)
    {
        const aiMesh* ai_mesh = scene->mMeshes[m_idx];
        std::string name = mesh_names[m_idx];

        Group group_item;
        group_item.offset = static_cast<u32>(index_data.size());
        group_item.length = ai_mesh->mNumFaces * 3;
        u32 len = std::min(static_cast<u32>(name.length()), 32U);
        strncpy(group_item.name, name.data(), len);
        group_item.name[len] = '\0';
        group_data.push_back(group_item);

        for (u32 v_idx = 0; v_idx < ai_mesh->mNumVertices; v_idx++)
        {
            if (num_bones > 0)
            {
                // Skinned format
                SkinnedVertex vertex;
                vec3 pos = *reinterpret_cast<glm::vec3*>(&ai_mesh->mVertices[v_idx]);
                vec3 normal = *reinterpret_cast<glm::vec3*>(&ai_mesh->mNormals[v_idx]);
                vec2 texcoord = ai_mesh->HasTextureCoords(0) ? *reinterpret_cast<glm::vec2*>(&ai_mesh->mTextureCoords[0][v_idx]) : vec2(0);
                
                vertex.position = pos;
                vertex.normal = glm::packSnorm3x10_1x2(vec4(normal, 0));
                vertex.texcoord = texcoord;
                
                skinned_vertex_data.push_back(vertex);
            }
            else
            {
                // Default format
                Vertex vertex;
                vec3 pos = *reinterpret_cast<glm::vec3*>(&ai_mesh->mVertices[v_idx]);
                vec3 normal = *reinterpret_cast<glm::vec3*>(&ai_mesh->mNormals[v_idx]);
                vec2 texcoord = ai_mesh->HasTextureCoords(0) ? *reinterpret_cast<glm::vec2*>(&ai_mesh->mTextureCoords[0][v_idx]) : vec2(0);
                
                vertex.position = pos;
                vertex.normal = normal;
                vertex.texcoord = texcoord;
                
                vertex_data.push_back(vertex);
            }
        }

        for (u32 f_idx = 0; f_idx < ai_mesh->mNumFaces; f_idx++)
        {
            index_data.push_back(vertex_offset + ai_mesh->mFaces[f_idx].mIndices[0]);
            index_data.push_back(vertex_offset + ai_mesh->mFaces[f_idx].mIndices[1]);
            index_data.push_back(vertex_offset + ai_mesh->mFaces[f_idx].mIndices[2]);
        }

        // Get bones with weights
        if (num_bones > 0) {

            //PrintBoneHierarchy(scene->mRootNode);
            // Read the hierarchy information for the bones
            if (m_idx == 0) {
                ReadBoneHierarchy(bone_data, scene->mRootNode, -1, glm::mat4());
                for (u32 i = 0; i < static_cast<u32>(bone_data.size()); i++) {
                    id_map.insert(std::pair<std::string, u32>(std::string(bone_data[i].name), i));
                }
            }
            
            for (u32 b_idx = 0; b_idx < ai_mesh->mNumBones; b_idx++) {
                // Get bone-ptr
                const aiBone* bone = ai_mesh->mBones[b_idx];

                // Store vertex weights for this bone
                for (unsigned int k = 0; k < bone->mNumWeights; ++k) {
                    const aiVertexWeight& vw = bone->mWeights[k];
                    //VertexBoneDataItem &vb = vertex_bone_data[vertex_offset + vw.mVertexId];
                    SkinnedVertex &vertex = skinned_vertex_data[vertex_offset + vw.mVertexId];
                    u32 idx = 0;
                    auto found = id_map.find(std::string(bone->mName.C_Str()));
                    if (found == id_map.end())
                        printf("No match found for bone '%s'.\n", bone->mName.C_Str());
                    else
                        idx = found->second;

                    InsertWeight(vertex, idx, vw.mWeight);
                }
            }
        }
        vertex_offset += ai_mesh->mNumVertices;
    }

    /*
    // Set any unused vertex bone index to 0
    if (num_bones > 0)
    {
        for (size_t i = 0; i < skinned_vertex_data.size(); i++) {
            for (size_t k = 0; k < 4; k++) {
                skinned_vertex_data[i].indices[k] = glm::max(0, skinned_vertex_data[i].indices[k]);
            }
        }
    }
     */
    
    u32 size_of_vertex = (num_bones > 0) ? sizeof(SkinnedVertex) : sizeof(Vertex);
    
    // Update the u32 counter of bone data
    num_bones = static_cast<u32>(bone_data.size());

    u32 vertex_data_offset = sizeof(MeshHeaderBlock);
    u32 index_data_offset = vertex_data_offset + static_cast<u32>(num_vertices * size_of_vertex);
    u32 group_data_offset = index_data_offset + static_cast<u32>(num_indices * sizeof(u32));
    u32 bone_data_offset = group_data_offset + static_cast<u32>(num_groups * sizeof(Group));

    const char* identifier = "GED_MESH";
    const u32 version = 1002U;
    const u32 vertex_format = num_bones > 0 ? 1 : 0;

    file.write(identifier, 8);
    file.write(reinterpret_cast<const char*>(&version), 4);
    file.write(reinterpret_cast<const char*>(&vertex_format), 4);

    file.write(reinterpret_cast<const char*>(&num_vertices), 4);
    file.write(reinterpret_cast<const char*>(&num_indices), 4);
    file.write(reinterpret_cast<const char*>(&num_groups), 4);
    file.write(reinterpret_cast<const char*>(&num_bones), 4);

    file.write(reinterpret_cast<const char*>(&vertex_data_offset), 4);
    file.write(reinterpret_cast<const char*>(&index_data_offset), 4);
    file.write(reinterpret_cast<const char*>(&group_data_offset), 4);
    file.write(reinterpret_cast<const char*>(&bone_data_offset), 4);

    if (vertex_format == 0)
        file.write(reinterpret_cast<const char*>(vertex_data.data()), num_vertices * size_of_vertex);
    else
        file.write(reinterpret_cast<const char*>(skinned_vertex_data.data()), num_vertices * size_of_vertex);
    file.write(reinterpret_cast<const char*>(index_data.data()), num_indices * sizeof(u32));
    file.write(reinterpret_cast<const char*>(group_data.data()), num_groups * sizeof(Group));
    file.write(reinterpret_cast<const char*>(bone_data.data()), num_bones * sizeof(Bone));

    file.close();

    return true;
}
/*
struct MeshDataBlock
{
    u64 identifier;
    u32 version;

    // HEADER
    u32 vertex_format
 
    u32 num_vertices;
    u32 num_indices;
    u32 num_groups;
    u32 num_bones;

    u32 vertex_data_offset;
    u32 vertex_bone_data_offset;
    u32 index_data_offset;
    u32 group_data_offset;
    u32 bone_data_offset;

    // Data follows here
};
*/