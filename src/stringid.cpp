#include <string.h>
#include "stringid.h"
#include "crc32.h"

namespace ged
{
    StringID::StringID(const char* string) :
        m_string(string),
        m_id(CRC32(string, strlen(string)))
    {}

    StringID::StringID(const std::string& string) :
        m_string(string),
        m_id(CRC32(string.c_str(), string.size()))
    {}

    StringID::StringID(const StringID& other) :
        m_string(other.m_string),
        m_id(other.m_id)
    {}

    StringID::StringID(const StringID&& other) :
        m_string(other.m_string),
        m_id(other.m_id)
    {}
}
