# [GED Exporter](https://bitbucket.org/scanberg/ged-exporter)

## Dependencies
GED Exporter uses the following dependencies (already included in the source package as git submodules).

Functionality             | Library
------------------------- | ------------------------------------------
Assimp                    | [assimp](https://github.com/assimp/assimp)

To pull all submodules from remote do

```git submodule update --init
```

or use the `--recursive` when cloning the repository.