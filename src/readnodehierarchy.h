#pragma once
#include <vector>
#include <assimp/mesh.h>
#include "common.h"

struct aiNode;

struct Node
{
    std::string name;
    i32 parent_idx = -1;
    i32 mesh_idx = -1;
    glm::mat4 parent_to_child;
};

void ReadNodeHierarchy(std::vector<Node>& node_data, const aiNode* node, i32 parent = -1);