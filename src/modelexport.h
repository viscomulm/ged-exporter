#pragma once
#include "readnodehierarchy.h"

struct aiScene;
bool ExportGEDModel(const char* dst_file, const aiScene* scene, const std::vector<std::string>& mesh_names);