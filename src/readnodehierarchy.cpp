#include "common.h"
#include "meshformat.h"
#include <vector>
#include <assimp/scene.h>
#include "readnodehierarchy.h"

static void CopyAiMatrixToGLM(const aiMatrix4x4 *from, glm::mat4 &to)
{
	// TODO Possibly just read and transpose

	to[0][0] = from->a1; to[1][0] = from->a2;
	to[2][0] = from->a3; to[3][0] = from->a4;
	to[0][1] = from->b1; to[1][1] = from->b2;
	to[2][1] = from->b3; to[3][1] = from->b4;
	to[0][2] = from->c1; to[1][2] = from->c2;
	to[2][2] = from->c3; to[3][2] = from->c4;
	to[0][3] = from->d1; to[1][3] = from->d2;
	to[2][3] = from->d3; to[3][3] = from->d4;
}

void ReadNodeHierarchy(std::vector<Node>& node_data,
	const aiNode* ai_node, i32 parent)
{
	if (ai_node == nullptr)
		return;

    Node node;

	node.name = std::string(ai_node->mName.C_Str());
    node.parent_idx = parent;
    if (ai_node->mNumMeshes > 0)
        node.mesh_idx = ai_node->mMeshes[0];
	CopyAiMatrixToGLM(&ai_node->mTransformation, node.parent_to_child);

    i32 idx = node_data.size();
    node_data.push_back(node);

	for (u32 i = 0; i < ai_node->mNumChildren; i++)
        ReadNodeHierarchy(node_data, ai_node->mChildren[i], idx);
}