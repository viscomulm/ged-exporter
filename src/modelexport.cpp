#include <assimp/scene.h>
#include <fstream>
#include <vector>
#include "common.h"
#include "modelexport.h"

bool ExportGEDModel(const char* dst_file, const aiScene* scene, const std::vector<std::string>& mesh_names)
{
    std::ofstream file(dst_file, std::fstream::out);
    if (!file.is_open()) {
        printf("Could not open or create model file! \n");
        exit(-1);
    }

    printf("Exporting model %s \n", dst_file);

    file << "# GED Model, generated through exporter" << std::endl << std::endl;

    file << "# Mesh file used." << std::endl;
    file << "MESH_FILE " << GetFileWithoutExtension(dst_file) + ".gmsh" << std::endl << std::endl;
    file << "# Material file used." << std::endl;
    file << "MATERIAL_FILE " << GetFileWithoutExtension(dst_file) + ".gmat" << std::endl << std::endl;

    std::vector<std::string> materials;
    if (scene->HasMaterials()) {
        for (u32 i = 0; i < scene->mNumMaterials; i++)
        {
            aiString ai_name;
            aiMaterial* mat = scene->mMaterials[i];
            if (mat) {
                mat->Get(AI_MATKEY_NAME, ai_name);
                std::string mat_name = ai_name.C_Str();
                if (mat_name == "")
                    mat_name = "material" + std::to_string(i);
                mat_name = RemoveWhiteSpace(mat_name);
                materials.push_back(mat_name);
            }
            else
                materials.push_back(std::string("material" + std::to_string(i)));
        }
    }

    if (scene->HasMeshes()) {
        file << "# Mappings of materials onto mesh groups" << std::endl;

        for (u32 i = 0; i < scene->mNumMeshes; i++)
        {
            aiMesh* ai_mesh = scene->mMeshes[i];
            file << "GROUP_MATERIAL ";
            file << mesh_names[i] << " " << materials[ai_mesh->mMaterialIndex] << std::endl;
        }
    }

    return true;
}
