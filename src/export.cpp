#include <cassert>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "export.h"
#include "common.h"

#include "modelexport.h"
#include "meshexport.h"
#include "materialexport.h"
#include "animationexport.h"

void ExportGED(const char* filename)
{
    assert(filename);

    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(filename,
        aiProcess_Triangulate |
        aiProcess_JoinIdenticalVertices |
        aiProcess_OptimizeMeshes |
        aiProcess_GenSmoothNormals |
        aiProcess_RemoveRedundantMaterials |
        aiProcess_LimitBoneWeights);

    printf("Trying to export '%s'. \n", filename);

    if (!scene) {
        printf("Could not open file: '%s' \n", importer.GetErrorString());
        return;
    }

    std::string file_extension = GetFileExtension(filename);
    std::string out_file = GetFile(filename);
    std::string out_file_wo_ext = GetFileWithoutExtension(filename);
    std::string out_path = GetPath(filename);
    std::string out_full_path_wo_ext = out_path + out_file_wo_ext;

    std::string model_file = out_full_path_wo_ext + ".gmdl";
    std::string mesh_file = out_full_path_wo_ext + ".gmsh";
    std::string mat_file = out_full_path_wo_ext + ".gmat";

    std::vector<Node> node_data;
    ReadNodeHierarchy(node_data, scene->mRootNode);

    std::vector<std::string> mesh_names(scene->mNumMeshes);
    for (auto i = 0; i < mesh_names.size(); i++)
        mesh_names[i] = "group" + std::to_string(i);
    for (const auto& n : node_data)
        if (n.mesh_idx > -1)
            mesh_names[n.mesh_idx] = RemoveWhiteSpace(n.name);;

    if (scene->HasAnimations()) {
        if (scene->mNumAnimations == 1) {
            const aiAnimation* ai_anim = scene->mAnimations[0];
            std::string anim_name(ai_anim->mName.C_Str());
            if (anim_name == "")
                anim_name = out_file_wo_ext;
            std::string anim_file = out_path + anim_name + ".gani";
            ExportGEDAnimation(anim_file.c_str(), ai_anim);
        }
        else
        {
            for (u32 i = 0; i < scene->mNumAnimations; i++)
            {
                const aiAnimation* ai_anim = scene->mAnimations[i];
                std::string anim_name(ai_anim->mName.C_Str());
                if (anim_name == "")
                    anim_name = out_file_wo_ext + std::to_string(i);
                std::string anim_file = out_path + anim_name + ".gani";
                ExportGEDAnimation(anim_file.c_str(), ai_anim);
            }
        }
    }

    // Identify possibly redundant information from md5 animation
    if (file_extension == "md5anim")
        return;

    if (scene->HasMeshes())
        ExportGEDMesh(mesh_file.c_str(), scene, mesh_names);

    if (scene->HasMaterials())
        ExportGEDMaterial(mat_file.c_str(), scene);

    ExportGEDModel(model_file.c_str(), scene, mesh_names);
}