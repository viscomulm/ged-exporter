#include <vector>
#include <fstream>
#include <assimp/anim.h>
#include "common.h"
#include "animationexport.h"
#include "stringid.h"

template<typename T>
struct KeyFrame
{
    float timestamp;
    T data;
};

struct Channel
{
    char name[32];

    u32 num_pos_frames;
    u32 num_rot_frames;
    u32 num_scl_frames;

    u32 frame_pos_index_offset;
    u32 frame_rot_index_offset;
    u32 frame_scl_index_offset;
};

struct Animation
{
    u64 identifier;
    u32 version;
    
    f32 frames_per_second;
    f32 duration;

    u32 num_channels;

    u32 channel_data_offset;
    u32 frame_pos_data_offset;
    u32 frame_rot_data_offset;
    u32 frame_scl_data_offset;
    // Channel data
    // Key frame data
};

bool ExportGEDAnimation(const char* dst_file, const aiAnimation* aiAnim)
{
    assert(dst_file);
    assert(aiAnim);

    std::ofstream file(dst_file, std::ofstream::binary);
    if (!file.is_open()) {
        printf("Could not open or create animation file! \n");
        return false;
    }

    printf("Exporting animation %s \n", dst_file);

    std::vector<Channel> channel_data;
    std::vector<KeyFrame<vec3>> frame_pos_data;
    std::vector<KeyFrame<quat>> frame_rot_data;
    std::vector<KeyFrame<vec3>> frame_scl_data;

    for (u32 i = 0; i < aiAnim->mNumChannels; i++) {
        aiNodeAnim* aiChannel = aiAnim->mChannels[i];

        printf("Channel [%u]: '%s'\n", i, aiChannel->mNodeName.C_Str());

        u32 pos_frame_offset = static_cast<u32>(frame_pos_data.size());
        u32 rot_frame_offset = static_cast<u32>(frame_rot_data.size());
        u32 scl_frame_offset = static_cast<u32>(frame_scl_data.size());

        u32 num_pos_frames = 0;
        u32 num_rot_frames = 0;
        u32 num_scl_frames = 0;
        
        KeyFrame<vec3> last_pos_frame { -1.f, vec3(FLT_MAX) };
        for (u32 k = 0; k < aiChannel->mNumPositionKeys; k++)
        {
            float timestamp = static_cast<f32>(aiChannel->mPositionKeys[k].mTime);
            const aiVector3D& aiPos = aiChannel->mPositionKeys[k].mValue;
            KeyFrame<vec3> frame { timestamp, vec3(aiPos.x, aiPos.y, aiPos.z)};

            if (frame.data != last_pos_frame.data) {
                if (last_pos_frame.timestamp > -1.f && frame_pos_data.size() > 0 && frame_pos_data.back().timestamp != last_pos_frame.timestamp)
                {
                    frame_pos_data.push_back(last_pos_frame);
                    num_pos_frames++;
                    printf("key_pos[%.2f]: %.7f %.7f %.7f \n", last_pos_frame.timestamp, last_pos_frame.data.x, last_pos_frame.data.y, last_pos_frame.data.z);
                }
                
                frame_pos_data.push_back(frame);
                num_pos_frames++;
                printf("key_pos[%.2f]: %.7f %.7f %.7f \n", frame.timestamp, frame.data.x, frame.data.y, frame.data.z);
            }
            
            last_pos_frame = frame;
        }

        KeyFrame<quat> last_rot_frame { -1.f, quat(0, 0, 0, 0) };
        for (u32 k = 0; k < aiChannel->mNumRotationKeys; k++)
        {
            float timestamp = static_cast<f32>(aiChannel->mRotationKeys[k].mTime);
            const aiQuaternion& aiRot = aiChannel->mRotationKeys[k].mValue;
            KeyFrame<quat> frame { timestamp, quat(aiRot.w, aiRot.x, aiRot.y, aiRot.z) };

            if (frame.data != last_rot_frame.data) {
                if (last_rot_frame.timestamp > -1.f && frame_rot_data.size() > 0 && frame_rot_data.back().timestamp != last_rot_frame.timestamp)
                {
                    frame_rot_data.push_back(last_rot_frame);
                    num_rot_frames++;
                    printf("key_rot[%.2f]: %.7f %.7f %.7f %.7f \n", last_rot_frame.timestamp, last_rot_frame.data.x, last_rot_frame.data.y, last_rot_frame.data.z, last_rot_frame.data.w);
                }
                frame_rot_data.push_back(frame);
                num_rot_frames++;
                printf("key_rot[%.2f]: %.7f %.7f %.7f %.7f \n", frame.timestamp, frame.data.x, frame.data.y, frame.data.z, frame.data.w);
            }
            
            last_rot_frame = frame;
        }

        KeyFrame<vec3> last_scl_frame { -1.f, vec3(FLT_MAX) };
        for (u32 k = 0; k < aiChannel->mNumScalingKeys; k++)
        {
            float timestamp = static_cast<f32>(aiChannel->mScalingKeys[k].mTime);
            const aiVector3D& aiScl = aiChannel->mScalingKeys[k].mValue;
            KeyFrame<vec3> frame { timestamp, vec3(aiScl.x, aiScl.y, aiScl.z) };

            if (frame.data != last_scl_frame.data) {
                if (last_scl_frame.timestamp > -1.f && frame_scl_data.size() > 0 && frame_scl_data.back().timestamp != last_scl_frame.timestamp)
                {
                    frame_scl_data.push_back(last_scl_frame);
                    num_scl_frames++;
                    printf("key_scl[%.2f]: %.5f %.5f %.5f \n", last_scl_frame.timestamp, last_scl_frame.data.x, last_scl_frame.data.y, last_scl_frame.data.z);
                }
                frame_scl_data.push_back(frame);
                num_scl_frames++;
                printf("key_scl[%.2f]: %.5f %.5f %.5f \n", frame.timestamp, frame.data.x, frame.data.y, frame.data.z);
            }
            
            last_scl_frame = frame;
        }

        Channel channel;
        u32 len = std::max(static_cast<u32>(aiChannel->mNodeName.length), 32U);
        strncpy(channel.name, aiChannel->mNodeName.C_Str(), len);
        channel.name[31] = '\0';
        channel.frame_pos_index_offset = pos_frame_offset;
        channel.frame_rot_index_offset = rot_frame_offset;
        channel.frame_scl_index_offset = scl_frame_offset;
        channel.num_pos_frames = num_pos_frames;
        channel.num_rot_frames = num_rot_frames;
        channel.num_scl_frames = num_scl_frames;

        channel_data.push_back(channel);
    }
    
    f32 frames_per_second = static_cast<f32>(aiAnim->mTicksPerSecond);
    f32 duration = static_cast<f32>(aiAnim->mDuration);
    u32 num_channels = channel_data.size();
    u32 channel_data_offset = sizeof(Animation);
    u32 frame_pos_data_offset = channel_data_offset + sizeof(Channel) * channel_data.size();
    u32 frame_rot_data_offset = frame_pos_data_offset + sizeof(KeyFrame<vec3>) * frame_pos_data.size();
    u32 frame_scl_data_offset = frame_rot_data_offset + sizeof(KeyFrame<quat>) * frame_rot_data.size();

    const char* identifier = "GED_ANIM";
    const u32 version = 1002U;

    file.write(identifier, 8);
    file.write(reinterpret_cast<const char*>(&version), 4);
    file.write(reinterpret_cast<const char*>(&frames_per_second), 4);
    file.write(reinterpret_cast<const char*>(&duration), 4);
    file.write(reinterpret_cast<const char*>(&num_channels), 4);
    file.write(reinterpret_cast<const char*>(&channel_data_offset), 4);
    file.write(reinterpret_cast<const char*>(&frame_pos_data_offset), 4);
    file.write(reinterpret_cast<const char*>(&frame_rot_data_offset), 4);
    file.write(reinterpret_cast<const char*>(&frame_scl_data_offset), 4);
    file.write(reinterpret_cast<const char*>(channel_data.data()), sizeof(Channel) * channel_data.size());
    file.write(reinterpret_cast<const char*>(frame_pos_data.data()), sizeof(KeyFrame<vec3>) * frame_pos_data.size());
    file.write(reinterpret_cast<const char*>(frame_rot_data.data()), sizeof(KeyFrame<quat>) * frame_rot_data.size());
    file.write(reinterpret_cast<const char*>(frame_scl_data.data()), sizeof(KeyFrame<vec3>) * frame_scl_data.size());

    file.close();

    return true;
}