#pragma once

enum ExportFlags
{
    EXPORT_INDIVIDUAL_MODELS = 1,
    EXPORT_INDIVIDUAL_MATERIALS = 2
};

void ExportGED(const char* filename);