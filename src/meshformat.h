#pragma once
#include "common.h"

struct Group
{
    char name[32];
    u32 offset;
    u32 length;
};

struct Bone
{
    char name[32];
    quat q;
    vec3 t;
    vec3 s;
    i32 parent;
};

struct Vertex
{
    vec3 position;
    vec3 normal;
    vec2 texcoord;
};

struct SkinnedVertex
{
    vec3 position;
    u32 normal;
    vec2 texcoord;
    u8vec4 indices = u8vec4(0);
    u8vec4 weights = u8vec4(0);
};

/*
struct VertexBoneDataItem
{
    ivec4 indices = ivec4(-1);
    vec4 weights;
};
 */

struct MeshHeaderBlock
{
    u64 identifier;
    u32 version;

    // HEADER
    u32 vertex_format;
    
    u32 num_vertices;
    u32 num_indices;
    u32 num_groups;
    u32 num_bones;

    u32 vertex_data_offset;
    u32 index_data_offset;
    u32 group_data_offset;
    u32 bone_data_offset;

    // Data follows here
};